using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectBuilding : MonoBehaviour
{
    [Header("References")]
    GameManager gameManager;

    public GameObject buildingUIPrefab;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void AddBuildingToList()
    {
        if (gameManager.buildingsToPlaceOnUI.Contains(buildingUIPrefab))
            return;

        if(gameManager.buildingsToPlaceOnUI.Count == 3)
        {
            gameManager.buildingsToPlaceOnUI.RemoveAt(0);
            gameManager.buildingsToPlaceOnUI.Add(buildingUIPrefab);
        }
        else
        {
            gameManager.buildingsToPlaceOnUI.Add(buildingUIPrefab);
        }

    }

}
