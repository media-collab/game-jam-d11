using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using Lean.Gui;

public class GameManager : MonoBehaviour
{
    [Header("References")]
    private StatsManager statsManager;
    private UIManager uiManager;
    private GridBuildingSystem gridBuildingSystem;
    private BuildingGhost buildingGhost;
    private Volume postProcessing;

    public bool level2;

    private bool hasLost;

    public GameObject loadingUI, strategy1, btnLevel2_Wave1, btn3;


    [Header("Wave Setting's")]
    public int currentWave;
    public List<GameObject> buildingsToPlaceOnUI = new List<GameObject>();
    public Transform buildingPos1, buildingPos2, buildingPos3;
    public List<Transform> wave1Pos = new List<Transform>();
    public List<Transform> wave2Pos = new List<Transform>();
    public List<Transform> wave3Pos = new List<Transform>();
    public List<GameObject> buildingsToBeSelected = new List<GameObject>();

    [Header("Strategy ID")]
    private int selectedStrategyID;
    public int amountEcoStrats;

    private void Awake()
    {
        statsManager = FindObjectOfType<StatsManager>();
        uiManager = FindObjectOfType<UIManager>();
        gridBuildingSystem = FindObjectOfType<GridBuildingSystem>();
        buildingGhost = FindObjectOfType<BuildingGhost>();
        postProcessing = FindObjectOfType<Volume>();
    }

    private void Update()
    {
        if (statsManager.currentMoney <= 0 && !hasLost)
        {
            uiManager.gameOverUI.SetActive(true);
            buildingGhost.DeselectBuilding();
            hasLost = true;
        }

        if (statsManager.currentCO2 > statsManager.maxCO2 * 0.5f)
        {
            if (postProcessing.profile.TryGet(out FilmGrain filmGrain))
            {
                float increment = (statsManager.currentCO2 - statsManager.maxCO2 * 0.5f) / (statsManager.maxCO2 * 0.5f);

                filmGrain.intensity.Override(increment);
            }
        }

        if (statsManager.currentCO2 >= 100 && !hasLost)
        {
            uiManager.gameOverUI.SetActive(true);
            buildingGhost.placedObjectTypeSO = null;
            Destroy(buildingGhost.visual.gameObject);
            buildingGhost.visual = null;
            buildingGhost.enabled = false;
            hasLost = true;

        }
    }

    public void DestroyChildObject(GameObject parent)
    {
        if (parent.transform.childCount > 0)
            Destroy(parent.transform.GetChild(0).gameObject);
    }

    public void LoadLevel2()
    {
        StartCoroutine(GoToLevel2());
    }

    public IEnumerator GoToLevel2()
    {
        buildingGhost.DeselectBuilding();
        yield return new WaitForSeconds(3f);
        loadingUI.SetActive(false);
        strategy1.GetComponent<LeanWindow>().TurnOn();
        btnLevel2_Wave1.SetActive(true);
        btn3.SetActive(false);
    }

    public void CurrentID(int id)
    {
        selectedStrategyID = id;
    }

    public void ConfirmStrategy()
    {
        switch (selectedStrategyID)
        {
            case 0:
                IndustrializationStrategy();
                break;
            case 1:
                EcoStrategy();
                break;
            case 2:
                TaxesStrategy();
                break;
        }
    }

    public void ConfirmBuildingsSelected(int waveID)
    {
        if (buildingsToPlaceOnUI.Count < 3)
        {
            //Show UI saying to pick 3
        }

        if (buildingsToPlaceOnUI.Count == 3)
        {
            Instantiate(buildingsToPlaceOnUI[0], buildingPos1);
            Instantiate(buildingsToPlaceOnUI[1], buildingPos2);
            Instantiate(buildingsToPlaceOnUI[2], buildingPos3);

            switch (waveID)
            {
                case 1:
                    uiManager.wave1.TurnOff();
                    break;
                case 2:
                    uiManager.wave2.TurnOff();
                    break;
                case 3:
                    uiManager.wave3.TurnOff();
                    break;
            }
        }
    }

    public void InstantiateRandomBuildingsWave1()
    {
        List<int> selectedNumbers = new List<int>();

        int random;

        for (int i = 0; i < 5; i++)
        {
            random = Random.Range(0, buildingsToBeSelected.Count);

            while (selectedNumbers.Contains(random))
            {
                random = Random.Range(0, buildingsToBeSelected.Count);
            }

            selectedNumbers.Add(random);
            Instantiate(buildingsToBeSelected[random], wave1Pos[i]);
        }
    }

    public void InstantiateRandomBuildingsWave2()
    {
        List<int> selectedNumbers = new List<int>();

        int random;

        for (int i = 0; i < 5; i++)
        {
            random = Random.Range(0, buildingsToBeSelected.Count);

            while (selectedNumbers.Contains(random))
            {
                random = Random.Range(0, buildingsToBeSelected.Count);
            }

            selectedNumbers.Add(random);
            Instantiate(buildingsToBeSelected[random], wave2Pos[i]);
        }
    }

    public void InstantiateRandomBuildingsWave3()
    {
        List<int> selectedNumbers = new List<int>();

        int random;

        for (int i = 0; i < 5; i++)
        {
            random = Random.Range(0, buildingsToBeSelected.Count);

            while (selectedNumbers.Contains(random))
            {
                random = Random.Range(0, buildingsToBeSelected.Count);
            }

            selectedNumbers.Add(random);
            Instantiate(buildingsToBeSelected[random], wave3Pos[i]);
        }
    }

    #region Strategy Event Functions

    private void IndustrializationStrategy()
    {
        statsManager.currentMoney += 2000;
        statsManager.currentCO2 += 30;
        uiManager.UpdateAllUI();
    }

    private void EcoStrategy()
    {
        statsManager.currentMoney -= 400;
        statsManager.currentCO2 -= 10;
        amountEcoStrats++;
        uiManager.UpdateAllUI();
    }

    private void TaxesStrategy()
    {
        statsManager.currentMoney += 1000;
        statsManager.currentCO2 += 15;
        statsManager.currentEnergy -= 15;
        statsManager.currentWater -= 15;
        uiManager.UpdateAllUI();
    }

    #endregion

    #region Bad Event Fuctions

    public void DestroyBuildings()
    {

        int random = Random.Range(1, 3);

        if (gridBuildingSystem.objectsOnGrid.Count < 3)
        {
            random = Random.Range(1, gridBuildingSystem.objectsOnGrid.Count - 1);
        }

        for (int i = 0; i < random; i++)
        {
            int randomObject = Random.Range(0, gridBuildingSystem.objectsOnGrid.Count - 1);

            PlacedObject placedObject = gridBuildingSystem.objectsOnGrid[randomObject];

            //Deemolish the object
            gridBuildingSystem.objectsOnGrid.Remove(placedObject);
            placedObject.DestroySelf();
            List<Vector2Int> gridPositionList = placedObject.GetGridPositionList();
            foreach (Vector2Int gridPosition in gridPositionList)
            {
                gridBuildingSystem.grid.GetGridObject(gridPosition.x, gridPosition.y).ClearPlacedObject();

            }


        }

    }

    #endregion

}
