using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class FirebaseManager : MonoBehaviour
{
    [Header("Save Score")]
    public InputField Name;
	public Text ScoreText;

    [Header("Firebase Config")]
	private DatabaseReference dbRef;
	private string userID;

    [Header("List of scores and names")]
    // Create list of text Text to store the data of name and score 
    public List<TextMeshProUGUI> nameTexts = new List<TextMeshProUGUI>();
    public List<TextMeshProUGUI> scoreTexts = new List<TextMeshProUGUI>();


    public void Awake()
    {
		userID= SystemInfo.deviceUniqueIdentifier;
        dbRef = FirebaseDatabase.DefaultInstance.RootReference;
	}

    public void Start()
    {
        GetUserInfo();
	}

 	public void CreateUser()
    {
		User new_user= new User(Name.text, int.Parse(ScoreText.text));
		string json = JsonUtility.ToJson(new_user);
		dbRef.Child("gjd").Child(userID).SetRawJsonValueAsync(json);
        Debug.Log("User created");
        GetUserInfo();
    }

	public void GetUserInfo() {
		FirebaseDatabase.DefaultInstance
		.GetReference("gjd")
		.GetValueAsync().ContinueWithOnMainThread(task => {
			if (task.IsFaulted) {
			// Handle the error...
                Debug.Log("Error");
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;

                // Save the data of name and score in list of text 
                List<string> nameList = new List<string>();
                List<string> scoreList = new List<string>();

                // create index
                int index = 0;
                // Get all the users from the database
                foreach (DataSnapshot user in snapshot.Children)
                {
                    // limit only top 10 users
                    if (index < 10)
                    {
                        // Save the data of name and score in list of text
                        nameList.Add(user.Child("name").Value.ToString());
                        scoreList.Add(user.Child("score").Value.ToString());
                        index++;
                    }
                    
                }

                // Sort the list of score by their name assosiated
                for (int i = 0; i < scoreList.Count; i++)
                {
                    for (int j = i + 1; j < scoreList.Count; j++)
                    {
                        if (int.Parse(scoreList[i]) < int.Parse(scoreList[j]))
                        {
                            string temp = scoreList[i];
                            scoreList[i] = scoreList[j];
                            scoreList[j] = temp;

                            temp = nameList[i];
                            nameList[i] = nameList[j];
                            nameList[j] = temp;
                        }
                    }
                }

                // Set the name and score to the text
                for (int i = 0; i < nameList.Count; i++)
                {
                    nameTexts[i].text = nameList[i];
                    scoreTexts[i].text = scoreList[i];
                }
			}
		});		
	}
}