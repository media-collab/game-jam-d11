using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGhost : MonoBehaviour
{
    public static BuildingGhost Instance { get; private set; }

    [HideInInspector] public Transform visual;
    [HideInInspector] public PlacedObjectTypeSO placedObjectTypeSO;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && placedObjectTypeSO != null)
        {
            DeselectBuilding();
        }
    }

    public void DeselectBuilding()
    {
        placedObjectTypeSO = null;
        if (visual != null)
            Destroy(visual.gameObject);
        visual = null;
    }

    private void LateUpdate()
    {

        if (placedObjectTypeSO != null)
        {

            Vector3 targetPosition = GridBuildingSystem.Instance.GetMouseWorldSnappedPosition(placedObjectTypeSO);
            targetPosition.y = 1f;
            transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * 15f);

            Quaternion rot = Quaternion.Euler(0, placedObjectTypeSO.GetRotationAngle(GridBuildingSystem.Instance.dir), 0);

            transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * 15f);
        }

    }

    public void InstantiateVisual(PlacedObjectTypeSO placedObjectTypeSO)
    {
        if (visual != null)
        {
            Destroy(visual.gameObject);
            visual = null;
        }

        if (placedObjectTypeSO != null)
        {
            visual = Instantiate(placedObjectTypeSO.visual, Vector3.zero, Quaternion.identity);
            visual.parent = transform;
            visual.localPosition = Vector3.zero;
            visual.localEulerAngles = Vector3.zero;
        }

        this.placedObjectTypeSO = placedObjectTypeSO;
    }

}
