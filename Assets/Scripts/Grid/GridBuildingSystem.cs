using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;


public class GridBuildingSystem : MonoBehaviour
{
    public static GridBuildingSystem Instance { get; private set; }

    [Header("References")]
    private StatsManager statsManager;
    private UIManager uiManager;
    private GoalManager goalManager;

    [Header("Grid & Placeable Objects Settings")]
    [HideInInspector] public GridXZ<GridObject> grid;
    [HideInInspector] public PlacedObjectTypeSO.Dir dir = PlacedObjectTypeSO.Dir.Down;
    private PlacedObjectTypeSO placedObjectTypeSO;
    public List<PlacedObject> objectsOnGrid = new List<PlacedObject>();

    public class GridObject
    {
        private GridXZ<GridObject> grid;
        private int x, z;
        private PlacedObject placedObject;

        public GridObject(GridXZ<GridObject> grid, int x, int z)
        {
            this.grid = grid;
            this.x = x;
            this.z = z;
            placedObject = null;
        }

        public void SetPlacedObject(PlacedObject placedObject)
        {
            this.placedObject = placedObject;
            grid.TriggerGridObjectChanged(x, z);
        }

        public PlacedObject GetPlacedObject()
        {
            return placedObject;
        }

        public void ClearPlacedObject()
        {
            placedObject = null;
            grid.TriggerGridObjectChanged(x, z);
        }

        public bool CanBuild()
        {
            return placedObject == null;
        }

        public override string ToString()
        {
            return x + "," + z;
        }
    }

    private void Awake()
    {
        Instance = this;

        int gridWidth = 10;
        int gridHeight = 10;
        float cellSize = 1f;
        grid = new GridXZ<GridObject>(gridWidth, gridHeight, cellSize, new Vector3(-5f, 0, -5f), (GridXZ<GridObject> g, int x, int z) => new GridObject(g, x, z));

        statsManager = FindObjectOfType<StatsManager>();
        uiManager = FindObjectOfType<UIManager>();
        goalManager = FindObjectOfType<GoalManager>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlacedObjectTypeSO placedObjectTypeSO = BuildingGhost.Instance.placedObjectTypeSO;

            if (placedObjectTypeSO != null)
                InstantiateBuilding(placedObjectTypeSO);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            dir = PlacedObjectTypeSO.GetNextDir(dir);
        }

        if (Input.GetMouseButtonDown(1))
        {
            GridObject gridObject = grid.GetGridObject(GetMouseWorldPosition());

            //Here we valid if theres actually a gridObject the that exactly position
            if (gridObject != null)
            {
                PlacedObject placedObject = gridObject.GetPlacedObject();

                if (placedObject != null)
                {
                    //Deemolish the object
                    objectsOnGrid.Remove(placedObject);
                    placedObject.DestroySelf();
                    List<Vector2Int> gridPositionList = placedObject.GetGridPositionList();
                    foreach (Vector2Int gridPosition in gridPositionList)
                    {
                        grid.GetGridObject(gridPosition.x, gridPosition.y).ClearPlacedObject();

                    }

                    PlacedObjectTypeSO placedObjectTypeSO = placedObject.GetPlacedObjectTypeSO();

                    statsManager.UpdateStats(Mathf.Abs(placedObjectTypeSO.moneyConsume), -placedObjectTypeSO.co2Consume,
                                Mathf.Abs(placedObjectTypeSO.energyConsume), Mathf.Abs(placedObjectTypeSO.waterConsume), -placedObjectTypeSO.score);

                    goalManager.amountOfBuildings--;
                    uiManager.UpdateAllUI();

                }
            }
        }
    }

    public static Vector3 GetMouseWorldPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit raycastHit))
        {
            return raycastHit.point;
        }
        else
        {
            return Vector3.zero;
        }
    }

    private void InstantiateBuilding(PlacedObjectTypeSO placedObjectTypeSO)
    {
        if (placedObjectTypeSO == null)
            return;

        grid.GetXZ(GetMouseWorldPosition(), out int x, out int z);

        Vector2Int placedObjectOrigin = new Vector2Int(x, z);
        placedObjectOrigin = grid.ValidateGridPosition(placedObjectOrigin);

        //Test if it can be build
        List<Vector2Int> gridPositionList = placedObjectTypeSO.GetGridPositionList(placedObjectOrigin, dir);
        bool canBuild = true;

        foreach (Vector2Int gridPosition in gridPositionList)
        {
            if (!grid.GetGridObject(gridPosition.x, gridPosition.y).CanBuild())
            {
                canBuild = false;
                break;
            }
        }

        if (!statsManager.HasEnoughMoneyToBuild(placedObjectTypeSO.moneyConsume))
        {
            UtilsClass.CreateWorldTextPopup("No tienes dinero!", GetMouseWorldPosition());
            return;
        }

        if (canBuild)
        {
            Vector2Int rotationOffset = placedObjectTypeSO.GetRotationOffset(dir);
            Vector3 placedObjectWorldPosition = grid.GetWorldPosition(placedObjectOrigin.x, placedObjectOrigin.y) + new Vector3(rotationOffset.x, 0, rotationOffset.y) * grid.GetCellSize();

            PlacedObject placedObject = PlacedObject.Create(placedObjectWorldPosition, placedObjectOrigin, dir, placedObjectTypeSO);

            objectsOnGrid.Add(placedObject);

            foreach (Vector2Int gridPosition in gridPositionList)
            {
                grid.GetGridObject(gridPosition.x, gridPosition.y).SetPlacedObject(placedObject);
            }

            statsManager.UpdateStats(placedObjectTypeSO.moneyConsume, placedObjectTypeSO.co2Consume,
                                        placedObjectTypeSO.energyConsume, placedObjectTypeSO.waterConsume, placedObjectTypeSO.score);

            goalManager.amountOfBuildings++;

            uiManager.UpdateAllUI();
        }
        else
        {
            UtilsClass.CreateWorldTextPopup("No se puede contruir!", GetMouseWorldPosition());
        }

    }

    public Vector3 GetMouseWorldSnappedPosition(PlacedObjectTypeSO placedObjectTypeSO)
    {
        Vector3 mousePosition = GetMouseWorldPosition();
        grid.GetXZ(mousePosition, out int x, out int z);

        if (placedObjectTypeSO != null)
        {
            Vector2Int rotationOffset = placedObjectTypeSO.GetRotationOffset(dir);
            Vector3 placedObjectWorldPosition = grid.GetWorldPosition(x, z) + new Vector3(rotationOffset.x, 0, rotationOffset.y) * grid.GetCellSize();
            return placedObjectWorldPosition;
        }
        else
        {
            return mousePosition;
        }
    }

    public Vector2Int GetGridPosition(Vector3 worldPosition)
    {
        grid.GetXZ(worldPosition, out int x, out int z);
        return new Vector2Int(x, z);
    }

}
