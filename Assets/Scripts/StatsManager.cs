using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    [Header("Money Stats")]
    public int maxMoney;
    public int currentMoney;

    [Header("C02 Stats")]
    public int maxCO2;
    public int currentCO2;

    [Header("Energy Stats")]
    public int maxEnergy;
    public int currentEnergy;

    [Header("Water Stats")]
    public int maxWater;
    public int currentWater;

    public int score;
    public Score textScore;

    private void Awake()
    {
        SetMaxMoney();
        SetStartCO2();
        SetMaxEnergy();
        SetMaxWater();
        SetStartScore();
    }

    private void Update()
    {
        if (textScore == null)
        {
            textScore = FindObjectOfType<Score>();

        }
        else
        {
            textScore.GetComponent<Text>().text = score.ToString();
        }
    }

    public void SetStartScore()
    {
        score = 0;
    }

    public void UpdateStats(int money, int co2, int energy, int water, int score)
    {
        UpdateMoney(money);
        UpdateCO2(co2);
        UpdateEnergy(energy);
        UpdateWater(water);
        UpdateScore(score);
    }

    public void UpdateScore(int value)
    {
        score += value;

        if (score < 0)
            score = 0;
    }

    #region Money Functions

    public void SetMaxMoney()
    {
        currentMoney = maxMoney;
    }

    public void UpdateMoney(int value)
    {
        currentMoney += value;

        if (currentMoney < 0)
            currentMoney = 0;
    }

    public bool HasEnoughMoneyToBuild(int costOfBuilding)
    {
        if (Mathf.Abs(costOfBuilding) > currentMoney)
            return false;
        else
            return true;
    }

    #endregion

    #region CO2 Functions

    public void SetStartCO2()
    {
        currentCO2 = 0;
    }

    public void SetMaxCO2()
    {
        currentCO2 = maxCO2;
    }

    public void UpdateCO2(int value)
    {
        currentCO2 += value;

        if (currentCO2 > maxCO2)
            currentCO2 = maxCO2;

        if (currentCO2 < 0)
            currentCO2 = 0;
    }

    #endregion

    #region Energy Functions

    public void SetMaxEnergy()
    {
        currentEnergy = maxEnergy;
    }

    public void UpdateEnergy(int value)
    {
        currentEnergy += value;

        if (currentEnergy > maxEnergy)
            currentEnergy = maxEnergy;

        if (currentEnergy < 0)
            currentEnergy = 0;
    }

    #endregion

    #region Water Functions

    public void SetMaxWater()
    {
        currentWater = maxWater;
    }

    public void UpdateWater(int value)
    {
        currentWater += value;


        if (currentWater > maxWater)
            currentWater = maxWater;

        if (currentWater < 0)
            currentWater = 0;
    }

    #endregion
}
