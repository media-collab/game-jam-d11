using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{

    #region SceneManagement
    public void ChangeScene(string sceneName)
    {
        switch (sceneName)
        {
            case "Menu":
                SceneManager.LoadScene("Menu");
                break;
            case "Game":
                SceneManager.LoadScene("LastGridSytemTesting");
                break;
            case "Rules":
                SceneManager.LoadScene("Rules");
                break;
            case "Credits":
                SceneManager.LoadScene("Credits");
                break;
            case "Result":
                ScoreManager.Instance.score = FindObjectOfType<StatsManager>().score;
                SceneManager.LoadScene("Result");
                break;
            default:
                break;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    #endregion
}
