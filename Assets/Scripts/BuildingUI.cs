using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Gui;
public class BuildingUI : MonoBehaviour
{
    [Header("References")]
    LeanButton leanButton;
    BuildingGhost buildingGhost;
    public PlacedObjectTypeSO placedObjectTypeSO;


    private void Start()
    {
        leanButton = GetComponent<LeanButton>();
        buildingGhost = FindObjectOfType<BuildingGhost>();
        leanButton.OnClick.AddListener(() => { buildingGhost.InstantiateVisual(placedObjectTypeSO); });
    }



}
