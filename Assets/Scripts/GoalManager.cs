using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour
{
    [Header("References")]
    private GameManager gameManager;
    private StatsManager statsManager;
    private UIManager uiManager;
    [Header("Parameteres")]
    public int amountOfBuildings;


    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        statsManager = FindObjectOfType<StatsManager>();
        uiManager = FindObjectOfType<UIManager>();
    }

    private void Update()
    {
        CO2Percentage_Goal();
        EcoStrategy();
        AmountOfBuildings_Goal();
        AmountOfMoney_Goal();
    }

    public void CO2Percentage_Goal()
    {
        if(statsManager.currentCO2 >= 80)
        {
            uiManager.goal1.color = Color.red;
        }
        else
        {
            uiManager.goal1.color = Color.green;
        }
    }

    public void EcoStrategy()
    {
        if (gameManager.amountEcoStrats >= 2)
        {
            uiManager.goal2.color = Color.green;
        }
        else
        {
            uiManager.goal2.color = Color.red;
        }

    }

    public void AmountOfBuildings_Goal()
    {
        if (amountOfBuildings >= 15)
        {
            uiManager.goal3.color = Color.green;
        }
        else
        {
            uiManager.goal3.color = Color.red;
        }
    }

    public void AmountOfMoney_Goal()
    {
        if (statsManager.currentMoney >= 200)
        {
            uiManager.goal4.color = Color.green;
        }
        else
        {
            uiManager.goal4.color = Color.red;

        }
    }
}
