using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Lean.Gui;

public class UIManager : MonoBehaviour
{
    [Header("References")]
    private StatsManager statsManager;

    [Header("HUD UI")]
    public GameObject pauseUI;
    public GameObject gameOverUI;
    public GameObject dialogUI;

    [Header("Goal UI References")]
    public TextMeshProUGUI goal1;
    public TextMeshProUGUI goal2;
    public TextMeshProUGUI goal3;
    public TextMeshProUGUI goal4;

    [Header("Waves UI")]
    public LeanWindow wave1, wave2, wave3;

    [Header("Money UI")]
    public TextMeshProUGUI moneyText;

    [Header("C02 UI")]
    public TextMeshProUGUI co2Text;

    [Header("Energy UI")]
    public TextMeshProUGUI energyText;

    [Header("Water UI")]
    public TextMeshProUGUI waterText;

    private void Awake()
    {
        statsManager = FindObjectOfType<StatsManager>();
    }

    private void Start()
    {
        #region Set Text Values

        moneyText.text = statsManager.maxMoney.ToString();
        co2Text.text = statsManager.currentCO2.ToString() + "%";
        energyText.text = statsManager.maxEnergy.ToString();
        waterText.text = statsManager.maxWater.ToString();


        #endregion
    }

    public void UpdateAllUI()
    {
        UpdateMoneyUI();
        UpdateCO2UI();
        UpdateEnergyUI();
        UpdateWaterUI();
    }

    #region Update UI Functions

    public void UpdateMoneyUI()
    {
        moneyText.text = statsManager.currentMoney.ToString();
    }

    public void UpdateCO2UI()
    {
        co2Text.text = statsManager.currentCO2.ToString() + "%";
    }

    public void UpdateEnergyUI()
    {
        energyText.text = statsManager.currentEnergy.ToString();
    }

    public void UpdateWaterUI()
    {
        waterText.text = statsManager.currentWater.ToString();
    }

    #endregion
}
